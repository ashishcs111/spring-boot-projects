package com.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class ProjectSecurityConfig {

	@Bean
	SecurityFilterChain defauSecurityFilterChain(HttpSecurity http) throws Exception{
		
		http.csrf((csrf)->csrf.disable())
		.authorizeHttpRequests((requests)->requests.requestMatchers("/home").permitAll()
				.requestMatchers("/contact").permitAll()
				.requestMatchers("/about").permitAll()
				.requestMatchers("/login").permitAll())
		.formLogin(loginConfigurer -> loginConfigurer.loginPage("/login").defaultSuccessUrl("/dashboard").failureUrl("/login?error=true").permitAll())
		.logout(logoutConfigurer->logoutConfigurer.logoutSuccessUrl("/logout?logout=true").invalidateHttpSession(true).permitAll())
		.httpBasic(Customizer.withDefaults());
		return http.build();
	}
}
