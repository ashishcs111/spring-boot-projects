package com.boot.config;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class WebConfig implements WebMvcConfigurer {
	
	public void addViewController(ViewControllerRegistry registry) {
		registry.addViewController("/home").setViewName("home");
		registry.addViewController("/about").setViewName("about"); 
		registry.addViewController("/contact").setViewName("contact"); 
		registry.addViewController("/login").setViewName("login"); 
	}
}
