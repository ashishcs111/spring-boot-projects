package com.boot.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TxService {
	
	@Pointcut("execution(public void com.boot.dao.EmployeeDao.*())")
	public void p1() {}
	
	@Pointcut("execution(public * com.boot.dao.EmployeeDao.returnType())")
	public void p2() {}
	
	@Pointcut("execution(public * com.boot.*.*.*())")
	public void p4() {}
	
//	@Pointcut("@annotation(com.boot.dao.EmployeeDao.annoataionName)")
//	public void p3() {}
	
	
	
	
	@Before("p1()")
	public void beginTx() {
		System.out.println("Tx Started");
	}
	
	@AfterReturning(value = "p2()", returning = "ob")
	public void returnTx(String ob) {
		System.out.println("tx returntype"+ ob);
	}
	
	@After("p1()")
	public void CommitTx() {
		System.out.println("Tx commited");
	}
	
	@AfterReturning("p1()")
	public void afterReturning() {
		System.out.println("after returning successfully");
	}
	
	@AfterThrowing(value = "p4()", throwing = "th")
	public void afterThrowing(Throwable th){
		System.out.println("after Throwing exception"+ th.getMessage());
	}
	
	
	@Around("p2()")
	public void aroundTest(ProceedingJoinPoint jp) {
		System.out.println("Before business logic");
		try {
			jp.proceed();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("After business logic");
	}
	
	
	
	
	

}
