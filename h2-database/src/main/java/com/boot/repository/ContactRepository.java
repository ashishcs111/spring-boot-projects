package com.boot.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.boot.model.Contact;

@Repository
@Component
public class ContactRepository {

//	@Autowired
	private JdbcTemplate jdbcTemplate;

	public ContactRepository() {
		super();
	}

	//	@Autowired
	public ContactRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int saveContactMsg(Contact contact) {
		String sql = "insert into contact_msg(name,mobile_num,email,subject,status,"+"created_at,created_by) values (?,?,?,?,?,?,?,?)";
		return jdbcTemplate.update(sql,contact.getName(),contact.getMobileNum(),contact.getEmail(),contact.getSubject(),contact.getMessage(),contact.getStatus(),contact.getCreatedAt(),contact.getCreatedBy());
	}
	
	

}
