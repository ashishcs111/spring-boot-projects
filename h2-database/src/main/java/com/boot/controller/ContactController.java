package com.boot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.boot.model.Contact;
import com.boot.service.ContactService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ContactController {
	
	private static Logger log = LoggerFactory.getLogger(ContactController.class);
	
	private final ContactService contactService;
	
	@Autowired
	public ContactController(ContactService contactService) {
		super();
		this.contactService = contactService;
	}

	@RequestMapping(value = {"/contact"})
	public String contact(Model model) { 
		model.addAttribute("contact", new Contact());
		return "contact";
	}
	
//	@RequestMapping(value = "/saveMsg", method = RequestMethod.POST)
//	public ModelAndView saveMessage(@ModelAttribute) {
//		
//		log.info("Name : "+name);
//		log.info("Mobile Number : "+mobileNum);
//		log.info("Email Address : "+name);
//		log.info("Subject : "+subject);
//		log.info("Message : "+message);
//		return new ModelAndView("redirect:/contact");
//		
//	}
	
	@RequestMapping(value = "/saveMsg", method = RequestMethod.POST)
 	public String saveMessage(@Valid @ModelAttribute("contact") Contact contact, Errors errors) {
		if(errors.hasErrors()) {
			log.error("Contact form validation failed due to : "+ errors.toString());
			return "contact";
		}
		contactService.saveMessageDetails(contact);
		return "redirect:/contact";
	}
	
}
