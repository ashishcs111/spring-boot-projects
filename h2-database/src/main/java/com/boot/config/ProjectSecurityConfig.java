package com.boot.config;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class ProjectSecurityConfig {

	@Bean
	SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {

		// Permit only specific urls or pages
//		http.csrf((csrf) -> csrf.disable())
		
		http.csrf((csrf) -> csrf.ignoringRequestMatchers("/saveMsg").ignoringRequestMatchers(PathRequest.toH2Console()))
				.authorizeHttpRequests(requests -> requests.requestMatchers("/dashboard").authenticated()
						.requestMatchers(HttpMethod.GET, "", "/", "/home").permitAll()
						.requestMatchers("/holidays/**").permitAll()
						.requestMatchers("/contact").permitAll()
						.requestMatchers("/saveMsg").permitAll()
//				 		.requestMatchers("/courses").authenticated()   this page needs credentials to be accessed
						.requestMatchers("/courses").permitAll().requestMatchers("/assets/**").permitAll()
						.requestMatchers("/about").permitAll()
						.requestMatchers("/login").permitAll()
						.requestMatchers("/logout").permitAll()
						.requestMatchers(PathRequest.toH2Console()).permitAll())
				.formLogin(loginConfigurer -> loginConfigurer.loginPage("/login").defaultSuccessUrl("/dashboard")
						.failureUrl("/login?error=true").permitAll())
				.logout(logoutConfigurer -> logoutConfigurer.logoutSuccessUrl("/login?logout=true")
						.invalidateHttpSession(true).permitAll())
				.httpBasic(Customizer.withDefaults());
		
				http.headers(headers -> headers.frameOptions(frame -> frame.disable()));

		// Permit All Requests inside the Web Application
//	        http.authorizeHttpRequests(requests -> requests.anyRequest().permitAll())
//	                .formLogin(Customizer.withDefaults())
//	                .httpBasic(Customizer.withDefaults());

		// Deny All Requests inside the Web Application
//	            http.authorizeHttpRequests(requests -> requests.anyRequest().denyAll())
//	                .formLogin(Customizer.withDefaults())
//	                .httpBasic(Customizer.withDefaults());

		return http.build();

	}

	@Bean
	public InMemoryUserDetailsManager userDetailsService() throws Exception {
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		UserDetails user = User.builder().username("user").password(passwordEncoder.encode("12345")).roles("USER").build();
		UserDetails admin = User.builder().username("admin").password(passwordEncoder.encode("54321")).roles("USER", "ADMIN")
				.build();

		return new InMemoryUserDetailsManager(user, admin);
	}

}
