package com.boot.constants;

public interface BootConstants {

	public static final String ANONYMOUS = "Anonymous";
	public static final String OPEN = "Open";
	public static final String CLOSE = "Close";
}
