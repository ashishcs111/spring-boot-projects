package com.boot.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

import com.boot.constants.BootConstants;
import com.boot.model.Contact;
import com.boot.repository.ContactRepository;

import lombok.extern.slf4j.Slf4j;


@Service
//@RequestScope    every request will be treated as new
//@SessionScope    one bean created per user
@ApplicationScope  //only one bean is created for everyuser
public class ContactService {
	
//	private static Logger log = LoggerFactory.getLogger(ContactService.class);
	 
	@Autowired
	private ContactRepository contactRepository;
	
	public ContactService() {
		System.out.println("Contact service bean initialized");
	}


	public boolean saveMessageDetails(Contact contact) {
		boolean isSaved = true;
		contact.setStatus(BootConstants.OPEN);
		contact.setCreatedBy(BootConstants.ANONYMOUS);
		contact.setCreatedAt(LocalDateTime.now());
		int result = contactRepository.saveContactMsg(contact);
		if(result>0) {
			isSaved = true;
		}   
		return isSaved;
	}

}
