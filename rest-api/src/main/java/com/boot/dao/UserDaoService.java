package com.boot.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.stereotype.Component;

import com.boot.Beans.User;

@Component
public class UserDaoService {


	public static List<User> users = new ArrayList<User>();
	
	public static int userCount = 0;
		
	static {
		users.add(new User(++userCount,"Ashish",LocalDate.now().minusYears(30)));
		users.add(new User(++userCount,"Manish",LocalDate.now().minusYears(20)));
		users.add(new User(++userCount,"Ram",LocalDate.now().minusYears(10)));
	}
	
	public List<User> findAll(){
		return users;
	}
	
	
	public User save(User user) {  
		user.setId(++userCount);
		users.add(user);
		return user;
		
	}
	
	
	public User findOne(int id) {
//	Predicate<? super User> predicate = user -> user.getId().equals(id);
	return users.stream().filter(user -> user.getId() == id).findFirst().orElse(null);
	}
	
	public void deleteById(int id) {
//		Predicate<? super User> predicate = user -> user.getId().equals(id);
		users.removeIf(user -> user.getId() == id);
		}
}
