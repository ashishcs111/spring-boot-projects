package com.boot.versioning;

public class PersonV1 {

	private String nameString;

	public PersonV1(String nameString) {
		super();
		this.nameString = nameString;
	}

	public String getNameString() {
		return nameString;
	}

	@Override
	public String toString() {
		return "PersonV1 [nameString=" + nameString + "]";
	}
	
	
}
