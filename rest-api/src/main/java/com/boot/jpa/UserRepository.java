package com.boot.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.boot.Beans.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	
}
