package com.boot.controller;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.boot.exception.NotImplementedException;

@Controller
public class EmployeeController {
	
	//3 ways of handling error and exeptions in spring
	//1 by adding error.html page in templates foler
	//2 by adding custom error pages 4xx.html and 5xx.html in temlpates/error folder
	//3 Controller advice 
	
	@GetMapping("/showa")
	public String showA() {
		return "welcome";
	}

	@GetMapping("/showb")
	public String showB() {
		if(new Random().nextInt(10)<=10)
			throw new NotImplementedException("Sample");
		return "welcome";
	}
	
	

}
