package com.boot.config;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class AnyCustomErrorController  implements ErrorController{

	@RequestMapping("/error")
	public String handleProblem(HttpServletRequest req) {
		int code = (Integer)req.getAttribute("javax.servlet.error.status_code");
		return "";
	}
	
	
	
	
	public String getErrorPath() {
		return "/error";
	}
}
