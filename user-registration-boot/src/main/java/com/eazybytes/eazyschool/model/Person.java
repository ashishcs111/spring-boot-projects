package com.eazybytes.eazyschool.model;

import org.hibernate.annotations.GenericGenerator;

import com.eazybytes.eazyschool.annotation.FieldValueMatch;
import com.eazybytes.eazyschool.annotation.PasswordValidator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
@FieldValueMatch.List({
	@FieldValueMatch(
			field = "pwd",
			fieldMatch = "confirmpwd",
			message = "Password do not match!"
			),
	@FieldValueMatch(
			field = "email",
			fieldMatch = "confirmEmail",
			message = "Email Address do not match!"
			)	
})
public class Person extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO,generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private int personId;
	
	@NotBlank(message = "Name must be not blank")
	@Size(min = 3, message = "Name must be at leat 3 character long")
	private String name;
	
	@NotBlank(message = "Mobile number must not be blank")
	@Pattern(regexp = "(^|[0-9]{10})",message = "mobile number nust be 10 digits")
	private String mobileNumber;
	
	@NotBlank(message = "Email number must not be blank")
	@Email(message = "Please provide a valid email address")
	private String email;
	
	@NotBlank(message = "Confirm Email number must not be blank")
	@Email(message = "Please provide a valid Confirm email address")
	@Transient
	private String confirmEmail;
	
	@NotBlank(message = "password must not be blank")
	@Size(min = 5, message = "Password must be at least 5 character long")
	@PasswordValidator()
	private int pwd;
	
	@NotBlank(message = "Confirm password must not be blank")
	@Size(min = 5, message = "Confirm Password must be at least 5 character long")
	@Transient
	private int confirmPwd;
}
