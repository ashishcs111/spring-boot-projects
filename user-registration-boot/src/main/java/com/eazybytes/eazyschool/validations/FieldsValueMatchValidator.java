package com.eazybytes.eazyschool.validations;

import org.springframework.beans.BeanWrapperImpl;

import com.eazybytes.eazyschool.annotation.FieldValueMatch;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class FieldsValueMatchValidator implements ConstraintValidator<FieldValueMatch, Object>{

	private String field;
	private String fieldMatch;
	
	@Override
	public void initialize(FieldValueMatch constraintAnnotation) {
		this.field = constraintAnnotation.field();
		this.fieldMatch = constraintAnnotation.fieldMatch();
	}
	
	
	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		 Object fieldValue = new BeanWrapperImpl(value).getPropertyValue(field);
		 Object fieldMatchValue = new BeanWrapperImpl(value).getPropertyValue(fieldMatch);
		if(fieldValue != null) {
			return fieldValue.equals(fieldMatchValue);
		}else {
			return fieldMatchValue == null;
		}
	}
	
	

}
