package com.boot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

import com.boot.model.Contact;

import lombok.extern.slf4j.Slf4j;


@Service
//@RequestScope    every request will be treated as new
//@SessionScope    one bean created per user
@ApplicationScope  //only one bean is created for everyuser
public class ContactService {
	
	private int counter = 0;
	
	private static Logger log = LoggerFactory.getLogger(ContactService.class);
	
	
	
	public ContactService() {
		System.out.println("Contact service bean initialized");
	}


	public boolean saveMessageDetails(Contact contact) {
		boolean isSaved = true;
		log.info(contact.toString());
		return isSaved;
	}


	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	public int getCounter() {
		return counter;
	}
	

}
