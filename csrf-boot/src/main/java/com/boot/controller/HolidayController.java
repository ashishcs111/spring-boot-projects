package com.boot.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.boot.model.Holiday;

import jakarta.websocket.server.PathParam;

@Controller
public class HolidayController {
	
	@RequestMapping("/holidays/{display}")
	public String displayHoliday(@PathVariable String display, Model model) {
		
		if(null!= display && display.equals("all")) {
			model.addAttribute("festival", true);
			model.addAttribute("federal", true);
		}else if(null!= display && display.equals("federal")) {
			model.addAttribute("federal", true);
		}else if(null!= display && display.equals("festival")) {
			model.addAttribute("festival", true);
		}
		
		
		List<Holiday> holidays = Arrays.asList(
				new Holiday("jan 1 ", "New Year",Holiday.Type.FESTIVAL),
				new Holiday("oct 31 ", "diwali",Holiday.Type.FESTIVAL),
				new Holiday("nov 24 ", "holi",Holiday.Type.FEDERAL),
				new Holiday("dec 25 ", "rakhi",Holiday.Type.FESTIVAL),
				new Holiday("jan 17 ", "ganesh chaturthi",Holiday.Type.FESTIVAL),
				new Holiday("july 4 ", "durga pooja",Holiday.Type.FEDERAL),
				new Holiday("sept 5 ", "sankranti",Holiday.Type.FESTIVAL),
				new Holiday("nov 11 ", "shivratri",Holiday.Type.FEDERAL)
				);
		
		Holiday.Type[] types = Holiday.Type.values();
		for(Holiday.Type type : types)
		{
			model.addAttribute(type.toString(),
					(holidays.stream().filter(holiday -> holiday.getType().equals(type)).collect(Collectors.toList())));
		}
		return "holidays.html";
	}

}
