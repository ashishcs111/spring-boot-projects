package com.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	
	@RequestMapping(value = {"","/","/home"})
	public String home(Model model) {
		model.addAttribute("username", "Ashish Patil");
		return "home";
	}
	
	@RequestMapping(value = {"/courses"})
	public String courses(Model model) {
		return "courses";
	}
	
	@RequestMapping(value = {"/about"})
	public String about(Model model) {
		return "about";
	}
	
}
