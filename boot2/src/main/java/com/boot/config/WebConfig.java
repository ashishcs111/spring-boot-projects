package com.boot.config;

import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class WebConfig implements WebMvcConfigurer {
	
	
	public void addViewController(ViewControllerRegistry registry) {
		registry.addViewController("/courses").setViewName("courses");
		registry.addViewController("/about").setViewName("about"); 
	}

}
 